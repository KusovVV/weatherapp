package com.gmail.victorkusov.weatherapp.utils;

public enum Units {

    FORMAT_FAHRENHEIHT {
        @Override
        public String toString() {
            return "imperial";
        }
    },
    FORMAT_CELSIUS {
        @Override
        public String toString() {
            return "metric";
        }
    },
    FORMAT_KELVIN {
        @Override
        public String toString() {
            return "default";
        }
    }
}
