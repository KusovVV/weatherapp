package com.gmail.victorkusov.weatherapp.network;

import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHelper {

    private static final String BASE_URL = "https://api.openweathermap.org";
    private static final String API_KEY_PARAMETR = "APPID";
    private static final String API_KEY_VALUE = "7cc4769bef3b1a9ffdce07dfe1807eb2";
    private static final String DATA_COUNT_PARAMETR = "cnt";
    private static final String DATA_COUNT_VALUE = "38";    // max value of rows

    private static IGetForecastData sInstance;


    private RetrofitHelper() {

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        HttpUrl url = request.url().newBuilder()
                                .addQueryParameter(API_KEY_PARAMETR, API_KEY_VALUE)
                                .addQueryParameter(DATA_COUNT_PARAMETR, DATA_COUNT_VALUE)
                                .build();
                        request = request.newBuilder().url(url).build();
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(new HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();


        Retrofit builder = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setLenient()
                        .create()))
                .client(client)
                .build();

        sInstance = builder.create(IGetForecastData.class);
    }

    public static synchronized IGetForecastData getInstance() {
        if (sInstance == null) {
            new RetrofitHelper();
        }
        return sInstance;
    }
}
