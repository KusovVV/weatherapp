package com.gmail.victorkusov.weatherapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;

public class Utils {


    public static boolean hasNetworkConnection(Context context) {
        boolean connected = true;
        ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (mConnectivityManager != null) {
            connected = !(mConnectivityManager.getActiveNetworkInfo() == null || !mConnectivityManager.getActiveNetworkInfo().isConnectedOrConnecting());
        }
        return connected;
    }


}
