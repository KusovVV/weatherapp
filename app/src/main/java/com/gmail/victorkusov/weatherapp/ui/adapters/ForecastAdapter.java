package com.gmail.victorkusov.weatherapp.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gmail.victorkusov.weatherapp.R;
import com.gmail.victorkusov.weatherapp.data.model.Forecast;
import com.gmail.victorkusov.weatherapp.data.model.Temperature;
import com.gmail.victorkusov.weatherapp.data.model.WeatherInfo;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.RVHolder> {

    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    List<Forecast> mForecastList;

    public List<Forecast> getForecastList() {
        return mForecastList;
    }

    public void setForecastList(List<Forecast> forecastList) {
        mForecastList = forecastList;
    }

    @NonNull
    @Override
    public RVHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler, parent, false);
        return new RVHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RVHolder holder, int position) {
        Forecast forecast = mForecastList.get(position);
        Context context = holder.mLogo.getContext();
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat inputFormat = new SimpleDateFormat(DATE_TIME_PATTERN);
            calendar.setTime(inputFormat.parse(forecast.getDateTime()));

            String day = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int month = calendar.get(Calendar.MONTH) + 1;
            int year = calendar.get(Calendar.YEAR) % 100;

            holder.mDateTime.setText(String.format(context.getString(R.string.item_date_time), day, hour, month, year));
        } catch (ParseException e) {
            holder.mDateTime.setText(R.string.date_time_parsing_error_message);
        }


        WeatherInfo weatherInfo = forecast.getWeatherInfo().get(0);

        Picasso.get().load(weatherInfo.getIconUri()).into(holder.mLogo);
        holder.mDescription.setText(weatherInfo.getDescription());

        String windBody = context.getResources().getString(R.string.item_wind);
        holder.mWindSpeed.setText(String.format(windBody, forecast.getWind().getSpeed()));

        int dirId = forecast.getWind().matchDirection();
        holder.mDirection.setText(context.getResources().getStringArray(R.array.item_wind_direction)[dirId]);

        Temperature temperature = forecast.getTemperature();
        holder.mTemperature.setText(String.format(context.getString(R.string.item_temperature), temperature.getTemperature()));
        holder.mHumidity.setText(String.format(context.getString(R.string.item_humidity), temperature.getHumidity()));
        holder.mPressure.setText(String.format(context.getString(R.string.item_pressure), temperature.getPressure()));
    }

    @Override
    public int getItemCount() {
        return mForecastList.size();
    }

    class RVHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.items_date)
        TextView mDateTime;

        @BindView(R.id.items_icon)
        ImageView mLogo;

        @BindView(R.id.items_description)
        TextView mDescription;

        @BindView(R.id.items_temperature)
        TextView mTemperature;

        @BindView(R.id.items_wind_speed)
        TextView mWindSpeed;

        @BindView(R.id.items_wind_direction)
        TextView mDirection;

        @BindView(R.id.items_humidity)
        TextView mHumidity;

        @BindView(R.id.items_pressure)
        TextView mPressure;

        RVHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
