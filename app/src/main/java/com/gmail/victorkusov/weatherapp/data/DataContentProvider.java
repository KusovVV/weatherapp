package com.gmail.victorkusov.weatherapp.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class DataContentProvider extends ContentProvider {

    private static final String AUTHORITY = "com.gmail.victorkusov.weatherapp";
    public static final String PATH_INFO = "Info";
    public static final String PATH_FORECAST = "Forecast";
    public static final Uri CONTENT_URI_INFO = Uri.parse("content://" + AUTHORITY + "/" + PATH_INFO);
    public static final Uri CONTENT_URI_FORECAST = Uri.parse("content://" + AUTHORITY + "/" + PATH_FORECAST);


    private static final String DB_NAME = "Weather";
    private static final int DB_VERSION = 1;

    private static final String TABLE_INFO = "Info";
    private static final String TABLE_FORECAST = "Forecast";

    private static final String TYPE_DOUBLE = "REAL";
    private static final String TYPE_INTEGER = "INTEGER";
    private static final String TYPE_STRING = "VARCHAR";

    public static final String TABLE_INFO_FIELD_ID = "id";
    public static final String TABLE_INFO_FIELD_NAME = "name";
    public static final String TABLE_INFO_FIELD_COUNTRY_CODE = "country_code";

    public static final String TABLE_FORECAST_FIELD_ID = "id";
    public static final String TABLE_FORECAST_FIELD_DATE_TIME = "dt_time";
    public static final String TABLE_FORECAST_FIELD_TEMPERATURE = "temperature";
    public static final String TABLE_FORECAST_FIELD_PRESSURE = "pressure";
    public static final String TABLE_FORECAST_FIELD_HUMIDITY = "humidity";
    public static final String TABLE_FORECAST_FIELD_WIND_SPEED = "wind_speed";
    public static final String TABLE_FORECAST_FIELD_WIND_ORIENTATION = "wind_orientation";
    public static final String TABLE_FORECAST_FIELD_MAIN_INFO = "info";
    public static final String TABLE_FORECAST_FIELD_MAIN_DESCRIPTION = "description";
    public static final String TABLE_FORECAST_FIELD_ICON_ID = "icon";

    private static final String TABLE_INFO_CREATION = "CREATE TABLE " + TABLE_INFO + "(" +
            TABLE_INFO_FIELD_ID + " " + TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT," +
            TABLE_INFO_FIELD_NAME + " " + TYPE_STRING + ", " +
            TABLE_INFO_FIELD_COUNTRY_CODE + " " + TYPE_STRING + ")";

    private static final String TABLE_FORECAST_CREATION = "CREATE TABLE " + TABLE_FORECAST + "(" +
            TABLE_FORECAST_FIELD_ID + " " + TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT," +
            TABLE_FORECAST_FIELD_DATE_TIME + " " + TYPE_STRING + ", " +
            TABLE_FORECAST_FIELD_TEMPERATURE + " " + TYPE_DOUBLE + ", " +
            TABLE_FORECAST_FIELD_PRESSURE + " " + TYPE_DOUBLE + ", " +
            TABLE_FORECAST_FIELD_HUMIDITY + " " + TYPE_INTEGER + ", " +
            TABLE_FORECAST_FIELD_WIND_SPEED + " " + TYPE_DOUBLE + ", " +
            TABLE_FORECAST_FIELD_WIND_ORIENTATION + " " + TYPE_DOUBLE + ", " +
            TABLE_FORECAST_FIELD_MAIN_INFO + " " + TYPE_STRING + ", " +
            TABLE_FORECAST_FIELD_MAIN_DESCRIPTION + " " + TYPE_STRING + ", " +
            TABLE_FORECAST_FIELD_ICON_ID + " " + TYPE_STRING + ")";

    private static final String TAG = DataContentProvider.class.getSimpleName();


    WeatherDBHelper helper;
    SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        helper = new WeatherDBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        String tableName = getTableName(uri.getLastPathSegment());
        Cursor query;
        db = helper.getReadableDatabase();
        try {
            db.beginTransaction();
            query = db.query(tableName, projection, selection, selectionArgs, null, null, sortOrder);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return query;
    }

    private String getTableName(String path) {
        String tableName = null;
        switch (path) {
            case PATH_FORECAST: {
                tableName = TABLE_FORECAST;
                break;
            }
            case PATH_INFO: {
                tableName = TABLE_INFO;
                break;
            }
        }
        return tableName;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        String tableName = getTableName(uri.getLastPathSegment());
        db = helper.getWritableDatabase();
        db.beginTransaction();
        try {
            db.insert(tableName, null, values);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        String tableName = getTableName(uri.getLastPathSegment());
        db = helper.getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(tableName, selection, selectionArgs);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }


    private class WeatherDBHelper extends SQLiteOpenHelper {


        private WeatherDBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TABLE_INFO_CREATION);
            db.execSQL(TABLE_FORECAST_CREATION);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(deleteTable(TABLE_INFO));
            db.execSQL(deleteTable(TABLE_FORECAST));
        }

        private String deleteTable(String tableName) {
            return "DROP TABLE IF EXISTS ".concat(tableName);
        }
    }
}
