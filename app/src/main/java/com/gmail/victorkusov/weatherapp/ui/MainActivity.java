package com.gmail.victorkusov.weatherapp.ui;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gmail.victorkusov.weatherapp.data.DataHandler;
import com.gmail.victorkusov.weatherapp.data.IOnDataReady;
import com.gmail.victorkusov.weatherapp.R;
import com.gmail.victorkusov.weatherapp.data.model.CityInfo;
import com.gmail.victorkusov.weatherapp.data.model.Forecast;
import com.gmail.victorkusov.weatherapp.data.model.ResponseData;
import com.gmail.victorkusov.weatherapp.data.model.WeatherInfo;
import com.gmail.victorkusov.weatherapp.ui.adapters.DataComparator;
import com.gmail.victorkusov.weatherapp.ui.adapters.ForecastAdapter;
import com.gmail.victorkusov.weatherapp.utils.Units;
import com.gmail.victorkusov.weatherapp.utils.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};

    private static final int REQUEST_LOCATION = 32;

    @BindView(R.id.recycler_container)
    RecyclerView mRecyclerView;

    @BindView(R.id.header_icon)
    ImageView mHeaderIcon;

    @BindView(R.id.img_empty)
    ImageView mEmptyImageNoData;

    @BindView(R.id.header_country_code)
    TextView mHeaderCountryCode;

    @BindView(R.id.header_town)
    TextView mHeaderTown;

    @BindView(R.id.header_temperature)
    TextView mHeaderTemperature;

    @BindView(R.id.anim_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbar;

    @BindView(R.id.appbar)
    AppBarLayout mAppBarLayout;

    @BindView(R.id.btn_reply)
    FloatingActionButton mActionButton;

    private Menu collapsedMenu;
    private boolean appBarExpanded = true;
    private IOnDataReady mDataReadyListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        setSupportActionBar(mToolbar);
        mCollapsingToolbar.setTitle(getString(R.string.app_name));

        mDataReadyListener = new IOnDataReady() {
            @Override
            public void onComplete(ResponseData data) {
                setupViews(data);
            }
        };

        mActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getForecastData();
            }
        });

        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) > 200) {
                    appBarExpanded = false;
                    invalidateOptionsMenu();
                } else {
                    appBarExpanded = true;
                    invalidateOptionsMenu();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        DataHandler instance = DataHandler.getInstance();
        instance.addOnDataReadyListener(mDataReadyListener);
        ResponseData data = instance.getData();
        if (data == null) {
            getForecastData();
        } else {
            setupViews(data);
        }
    }

    @Override
    protected void onStop() {
        DataHandler.getInstance().removeOnDataReadyListener(mDataReadyListener);
        super.onStop();
    }

    private void getForecastData() {
        if (Utils.hasNetworkConnection(MainActivity.this)) {
            getForecastFromNetwork();
        } else {
            Toast.makeText(this, R.string.no_network_connection, Toast.LENGTH_LONG).show();
            DataHandler.getInstance().refreshData(this);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (collapsedMenu != null
                && (!appBarExpanded || collapsedMenu.size() != 0)) {
            collapsedMenu.add("Reply")
                    .setIcon(R.drawable.ic_replay)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }
        return super.onPrepareOptionsMenu(collapsedMenu);
    }


    private void getForecastFromNetwork() {
        FusedLocationProviderClient locationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, PERMISSIONS[0]) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, PERMISSIONS[1]) == PackageManager.PERMISSION_GRANTED) {

            locationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    DataHandler.getInstance().refreshData(MainActivity.this,
                            location.getLatitude(), location.getLongitude(),
                            String.valueOf(Units.FORMAT_CELSIUS), getString(R.string.request_data_language));
                }
            });
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, REQUEST_LOCATION);
            }
        }
    }

    private void setupAdapter(List<Forecast> data) {
        ForecastAdapter forecastAdapter = (ForecastAdapter) mRecyclerView.getAdapter();
        if (forecastAdapter == null) {
            forecastAdapter = new ForecastAdapter();
            forecastAdapter.setForecastList(data);
            mRecyclerView.setAdapter(forecastAdapter);
        } else {
            List<Forecast> oldData = forecastAdapter.getForecastList();
            DataComparator comparator = new DataComparator(oldData, data);
            DiffUtil.DiffResult compareResult = DiffUtil.calculateDiff(comparator);
            forecastAdapter.setForecastList(data);
            compareResult.dispatchUpdatesTo(forecastAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        collapsedMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        getForecastData();
        return true;
    }

    private void setupViews(ResponseData data) {
        if (data.getForecastList() == null || data.getCity() == null) {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyImageNoData.setVisibility(View.VISIBLE);
            return;
        }
        mRecyclerView.setVisibility(View.VISIBLE);
        mEmptyImageNoData.setVisibility(View.GONE);

        CityInfo cityInfo = data.getCity();
        mHeaderCountryCode.setText(cityInfo.getCountryCode());
        mHeaderTown.setText(cityInfo.getCityName());

        List<Forecast> forecastList = data.getForecastList();
        Forecast forecast = forecastList.get(0);

        float strTemp = forecast.getTemperature().getTemperature();
        WeatherInfo weatherInfo = forecast.getWeatherInfo().get(0);
        String description = weatherInfo.getDescription();

        mHeaderTemperature.setText(String.format(getString(R.string.header_temp_info), description, strTemp));

        Picasso.get().load(weatherInfo.getIconUri()).into(mHeaderIcon);

        setupAdapter(forecastList.subList(1, forecastList.size()));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_LOCATION) {
            if (permissions.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getForecastData();
            } else {
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle(R.string.dlg_permission_denied_title)
                        .setMessage(R.string.dlg_permission_denied_message)
                        .setPositiveButton(R.string.dlg_permission_denied_btn_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(MainActivity.this, getResources().getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();
                                MainActivity.this.finish();
                            }
                        });
                dialog.show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
