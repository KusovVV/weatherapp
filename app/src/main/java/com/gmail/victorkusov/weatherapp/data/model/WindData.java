package com.gmail.victorkusov.weatherapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class WindData {

//    "wind": {
//        "speed": 5.82,
//                "deg": 45.5026
//    },

    @SerializedName("speed")
    private float speed;

    @SerializedName("deg")
    private float orientation;

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getOrientation() {
        return orientation;
    }

    public void setOrientation(float orientation) {
        this.orientation = orientation;
    }

    @Override
    public String toString() {
        return "WindData{" +
                "speed=" + speed +
                ", orientation=" + orientation +
                '}';
    }

    public int matchDirection() {
        int res = 0;
        for (float i = orientation - 90; i > 0; i -= 90) {
            res++;
        }
        return res;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WindData windData = (WindData) o;
        return Float.compare(windData.speed, speed) == 0 &&
                Float.compare(windData.orientation, orientation) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(speed, orientation);
    }
}
