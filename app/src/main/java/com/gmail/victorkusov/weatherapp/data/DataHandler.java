package com.gmail.victorkusov.weatherapp.data;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.gmail.victorkusov.weatherapp.data.model.CityInfo;
import com.gmail.victorkusov.weatherapp.data.model.Forecast;
import com.gmail.victorkusov.weatherapp.data.model.ResponseData;
import com.gmail.victorkusov.weatherapp.data.model.Temperature;
import com.gmail.victorkusov.weatherapp.data.model.WeatherInfo;
import com.gmail.victorkusov.weatherapp.data.model.WindData;
import com.gmail.victorkusov.weatherapp.network.RetrofitHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataHandler {

    private static final String TAG = DataHandler.class.getSimpleName();
    private ResponseData mData;
    private static DataHandler instance;
    private List<IOnDataReady> listeners;

    public static synchronized DataHandler getInstance() {
        if (instance == null) {
            instance = new DataHandler();
        }
        return instance;
    }

    private DataHandler() {

    }

    public ResponseData getData() {
        return mData;
    }

    public void refreshData(final Context context, double latitude, double longitude, String format, String language) {
        Call<ResponseData> call = RetrofitHelper.getInstance()
                .getForecastData(latitude, longitude, format, language);

        call.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(@NonNull Call<ResponseData> call, @NonNull Response<ResponseData> response) {
                mData = response.body();
                saveDataToSQLite(context);
                notifyDataChanged();
            }

            @Override
            public void onFailure(@NonNull Call<ResponseData> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }

    public void refreshData(final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mData = getDataFromSQLite(context);
                Handler handler = new Handler(context.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataChanged();
                    }
                });
            }
        }).start();

    }

    public void addOnDataReadyListener(IOnDataReady listener) {
        if (listeners == null) {
            listeners = new ArrayList<>();
        }
        listeners.add(listener);
    }

    public void removeOnDataReadyListener(IOnDataReady listener) {
        listeners.remove(listener);
    }

    private ResponseData getDataFromSQLite(Context context) {
        ResponseData data = new ResponseData();
        Cursor query = context.getContentResolver().query(DataContentProvider.CONTENT_URI_INFO,
                null, null, null, null);
        if (query != null && query.getCount() != 0) {
            query.moveToFirst();

            CityInfo cityInfo = new CityInfo();
            cityInfo.setCityName(query.getString(query.getColumnIndex(DataContentProvider.TABLE_INFO_FIELD_NAME)));
            cityInfo.setCountryCode(query.getString(query.getColumnIndex(DataContentProvider.TABLE_INFO_FIELD_COUNTRY_CODE)));
            data.setCity(cityInfo);

            query.close();
        }

        query = context.getContentResolver().query(DataContentProvider.CONTENT_URI_FORECAST,
                null, null, null, null);
        if (query != null && query.getCount() != 0) {
            List<Forecast> forecastList = new ArrayList<>(query.getCount());
            query.moveToFirst();
            do {
                Forecast forecast = new Forecast();
                forecast.setDateTime(query.getString(query.getColumnIndex(DataContentProvider.TABLE_FORECAST_FIELD_DATE_TIME)));

                Temperature temperature = new Temperature();
                temperature.setTemperature(query.getFloat(query.getColumnIndex(DataContentProvider.TABLE_FORECAST_FIELD_TEMPERATURE)));
                temperature.setHumidity(query.getShort(query.getColumnIndex(DataContentProvider.TABLE_FORECAST_FIELD_HUMIDITY)));
                temperature.setPressure(query.getFloat(query.getColumnIndex(DataContentProvider.TABLE_FORECAST_FIELD_PRESSURE)));
                forecast.setTemperature(temperature);

                WindData windData = new WindData();
                windData.setSpeed(query.getFloat(query.getColumnIndex(DataContentProvider.TABLE_FORECAST_FIELD_WIND_SPEED)));
                windData.setOrientation(query.getFloat(query.getColumnIndex(DataContentProvider.TABLE_FORECAST_FIELD_WIND_ORIENTATION)));
                forecast.setWind(windData);

                List<WeatherInfo> weatherInfo = new ArrayList<>(1);
                WeatherInfo info = new WeatherInfo();
                info.setMainInfo(query.getString(query.getColumnIndex(DataContentProvider.TABLE_FORECAST_FIELD_MAIN_INFO)));
                info.setDescription(query.getString(query.getColumnIndex(DataContentProvider.TABLE_FORECAST_FIELD_MAIN_DESCRIPTION)));
                info.setIconId(query.getString(query.getColumnIndex(DataContentProvider.TABLE_FORECAST_FIELD_ICON_ID)));
                weatherInfo.add(info);
                forecast.setWeatherInfo(weatherInfo);
                forecastList.add(forecast);
            } while (query.moveToNext());
            query.close();
            data.setForecastList(forecastList);
        }
        return data;
    }

    private void saveDataToSQLite(final Context context) {
        if (mData == null) {
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {

                ContentResolver resolver = context.getContentResolver();

                resolver.delete(DataContentProvider.CONTENT_URI_FORECAST, null, null);
                resolver.delete(DataContentProvider.CONTENT_URI_INFO, null, null);

                ContentValues valuesInfo = new ContentValues();

                CityInfo cityInfo = mData.getCity();
                valuesInfo.put(DataContentProvider.TABLE_INFO_FIELD_NAME, cityInfo.getCityName());
                valuesInfo.put(DataContentProvider.TABLE_INFO_FIELD_COUNTRY_CODE, cityInfo.getCountryCode());
                resolver.insert(DataContentProvider.CONTENT_URI_INFO, valuesInfo);

                List<Forecast> forecasts = mData.getForecastList();
                List<ContentValues> values = new ArrayList<>(forecasts.size());
                for (Forecast forecast : forecasts) {
                    ContentValues value = new ContentValues();

                    value.put(DataContentProvider.TABLE_FORECAST_FIELD_DATE_TIME, forecast.getDateTime());

                    Temperature temperature = forecast.getTemperature();
                    value.put(DataContentProvider.TABLE_FORECAST_FIELD_TEMPERATURE, temperature.getTemperature());
                    value.put(DataContentProvider.TABLE_FORECAST_FIELD_PRESSURE, temperature.getPressure());
                    value.put(DataContentProvider.TABLE_FORECAST_FIELD_HUMIDITY, temperature.getHumidity());

                    WindData wind = forecast.getWind();
                    value.put(DataContentProvider.TABLE_FORECAST_FIELD_WIND_SPEED, wind.getSpeed());
                    value.put(DataContentProvider.TABLE_FORECAST_FIELD_WIND_ORIENTATION, wind.getOrientation());

                    WeatherInfo info = forecast.getWeatherInfo().get(0);
                    value.put(DataContentProvider.TABLE_FORECAST_FIELD_MAIN_INFO, info.getMainInfo());
                    value.put(DataContentProvider.TABLE_FORECAST_FIELD_MAIN_DESCRIPTION, info.getDescription());
                    value.put(DataContentProvider.TABLE_FORECAST_FIELD_ICON_ID, info.getIconId());

                    values.add(value);
                }

                for (ContentValues value : values) {
                    resolver.insert(DataContentProvider.CONTENT_URI_FORECAST, value);
                }
            }
        }).start();
    }

    private void notifyDataChanged() {
        for (IOnDataReady listener : listeners) {
            listener.onComplete(mData);
        }
    }
}
