package com.gmail.victorkusov.weatherapp.data.model;

import android.net.Uri;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class WeatherInfo {
    private static final String ICON_URI = "http://openweathermap.org/img/w/%1$s.png";

//            "weather": [
//    {
//        "id": 800,
//            "main": "Clear",
//            "description": "clear sky",
//            "icon": "02d"
//    }
//            ],

    @SerializedName("main")
    private String mainInfo;

    @SerializedName("description")
    private String description;

    @SerializedName("icon")
    private String iconId;

    public String getMainInfo() {
        return mainInfo;
    }

    public void setMainInfo(String mainInfo) {
        this.mainInfo = mainInfo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconId() {
        return iconId;
    }

    public Uri getIconUri() {
        return Uri.parse(String.format(ICON_URI, iconId));
    }

    public void setIconId(String iconId) {
        this.iconId = iconId;
    }

    @Override
    public String toString() {
        return "WeatherInfo{" +
                "mainInfo='" + mainInfo + '\'' +
                ", description='" + description + '\'' +
                ", iconId='" + iconId + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherInfo that = (WeatherInfo) o;
        return Objects.equals(mainInfo, that.mainInfo) &&
                Objects.equals(description, that.description) &&
                Objects.equals(iconId, that.iconId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(mainInfo, description, iconId);
    }
}
