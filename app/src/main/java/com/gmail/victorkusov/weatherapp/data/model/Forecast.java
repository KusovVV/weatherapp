package com.gmail.victorkusov.weatherapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Forecast {

//    "dt": 1526148000,
//            "main": {
//                  "temp": ...
//                  "dt_txt": ...
//                  "wind": ...
//                  "weather": [ ... ]
//                  }

    @SerializedName("main")
    private Temperature mTemperature;

    @SerializedName("wind")
    private WindData wind;

    @SerializedName("dt_txt")
    private String dateTime;

    @SerializedName("weather")
    List<WeatherInfo> weatherInfo;

    public Temperature getTemperature() {
        return mTemperature;
    }

    public void setTemperature(Temperature temperature) {
        mTemperature = temperature;
    }

    public WindData getWind() {
        return wind;
    }

    public void setWind(WindData wind) {
        this.wind = wind;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public List<WeatherInfo> getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(List<WeatherInfo> weatherInfo) {
        this.weatherInfo = weatherInfo;
    }

    @Override
    public String toString() {
        return "Forecast{" +
                "mTemperature=" + mTemperature +
                ", wind=" + wind +
                ", dateTime='" + dateTime + '\'' +
                ", weatherInfo=" + weatherInfo +
                '}';
    }
}
