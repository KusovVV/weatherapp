package com.gmail.victorkusov.weatherapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseData {

    @SerializedName("city")
    private CityInfo city;

    @SerializedName("list")
    List<Forecast> mForecastList;

    public CityInfo getCity() {
        return city;
    }

    public void setCity(CityInfo city) {
        this.city = city;
    }

    public List<Forecast> getForecastList() {
        return mForecastList;
    }

    public void setForecastList(List<Forecast> forecastList) {
        mForecastList = forecastList;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "city=" + city +
                ", mForecastList=" + mForecastList +
                '}';
    }
}
