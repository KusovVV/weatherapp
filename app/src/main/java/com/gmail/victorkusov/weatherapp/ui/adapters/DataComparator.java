package com.gmail.victorkusov.weatherapp.ui.adapters;

import android.support.v7.util.DiffUtil;

import com.gmail.victorkusov.weatherapp.data.model.Forecast;
import com.gmail.victorkusov.weatherapp.data.model.WeatherInfo;

import java.util.List;

public class DataComparator extends DiffUtil.Callback {

    private final List<Forecast> oldList;
    private final List<Forecast> newList;

    public DataComparator(List<Forecast> oldList, List<Forecast> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        String oldDate = oldList.get(oldItemPosition).getDateTime();
        String newDate = newList.get(newItemPosition).getDateTime();
        return oldDate.equals(newDate);
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        boolean isEqual;

        Forecast oldElement = oldList.get(oldItemPosition);
        Forecast newElement = newList.get(newItemPosition);

        WeatherInfo oldInfo = oldElement.getWeatherInfo().get(0);
        WeatherInfo newInfo = newElement.getWeatherInfo().get(0);

        isEqual = oldInfo.equals(newInfo) &&
                oldElement.getTemperature().equals(newElement.getTemperature()) &&
                oldElement.getWind().equals(newElement.getWind());

        return isEqual;
    }
}
