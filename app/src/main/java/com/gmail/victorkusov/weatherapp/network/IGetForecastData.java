package com.gmail.victorkusov.weatherapp.network;

import com.gmail.victorkusov.weatherapp.data.model.ResponseData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IGetForecastData {

    String LATITUDE = "lat";
    String LONGITUDE = "lon";
    String UNITS = "units";
    String LANGUAGE = "lang";

    @GET("/data/2.5/forecast")
    Call<ResponseData> getForecastData(@Query(LATITUDE) double latitude,
                                       @Query(LONGITUDE) double longitude,
                                       @Query(UNITS) String unitsFormat,
                                       @Query(LANGUAGE) String language);

}
