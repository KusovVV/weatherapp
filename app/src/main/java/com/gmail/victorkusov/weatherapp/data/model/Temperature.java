package com.gmail.victorkusov.weatherapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Temperature {

//    "main": {
//        "temp": 19.42,
//                "temp_min": 19.42,
//                "temp_max": 19.42,
//                "pressure": 1004.56,
//                "sea_level": 1033.04,
//                "grnd_level": 1004.56,
//                "humidity": 30,
//                "temp_kf": 0
//    },

    @SerializedName("temp")
    private float temperature;

    @SerializedName("pressure")
    private float pressure;

    @SerializedName("humidity")
    private Short humidity;

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public Short getHumidity() {
        return humidity;
    }

    public void setHumidity(Short humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "Temperature{" +
                "temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Temperature that = (Temperature) o;
        return Float.compare(that.temperature, temperature) == 0 &&
                Float.compare(that.pressure, pressure) == 0 &&
                Objects.equals(humidity, that.humidity);
    }

    @Override
    public int hashCode() {

        return Objects.hash(temperature, pressure, humidity);
    }
}
