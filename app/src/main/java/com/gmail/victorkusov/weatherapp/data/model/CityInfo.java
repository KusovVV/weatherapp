package com.gmail.victorkusov.weatherapp.data.model;

import com.google.gson.annotations.SerializedName;

public class CityInfo {

    @SerializedName("name")
    private String cityName;

    @SerializedName("country")
    private String countryCode;

    public CityInfo() {
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "CityInfo{" +
                "cityName='" + cityName + '\'' +
                ", countryCode='" + countryCode + '\'' +
                '}';
    }
}
