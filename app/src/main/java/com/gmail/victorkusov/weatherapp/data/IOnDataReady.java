package com.gmail.victorkusov.weatherapp.data;

import com.gmail.victorkusov.weatherapp.data.model.ResponseData;

public interface IOnDataReady {

    void onComplete(ResponseData data);
}
